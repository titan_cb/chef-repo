name "web_server"
description "just a test role to configure some webserver"


run_list "recipe[apt]", "recipe[nginx]"

env_run_lists "_default" => ["recipe[nginx::config_prod]"], "testing" => ["recipe[nginx::config_test]"]



default_attributes "nginx" => { "log_location" => "/var/log/nginx.log" }
override_attributes "nginx" => { "gzip" => "on" }
