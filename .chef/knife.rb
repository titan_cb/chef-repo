
# Got this from http://docs.opscode.com/config_rb_knife.html
# some adaptions towards our needs

current_dir = File.dirname(__FILE__)
user = ENV['OPSCODE_USER'] || ENV['USER']
node_name                user
client_key               "#{current_dir}/#{user}.pem"
validation_client_name   "#{ENV['ORGNAME']}-validator"
validation_key           "#{current_dir}/#{ENV['ORGNAME']}-validator.pem"
chef_server_url          "http://lnx0119b.m086:8108/"
  
syntax_check_cache_path  "#{current_dir}/syntax_check_cache"
cookbook_path            ["#{current_dir}/../cookbooks"]
cookbook_copyright "ARZ Allgemeines Rechenzentrum GmbH"
cookbook_license "apachev2"
cookbook_email "christian.bitschnau@arz.at"

  # Amazon AWS
  # knife[:aws_access_key_id] = ENV['AWS_ACCESS_KEY_ID']
  # knife[:aws_secret_access_key] = ENV['AWS_SECRET_ACCESS_KEY']

  # Rackspace Cloud
  # knife[:rackspace_api_username] = ENV['RACKSPACE_USERNAME']
  # knife[:rackspace_api_key] = ENV['RACKSPACE_API_KEY']
  
knife[:editor] = "vim"


http_proxy      ENV['http_proxy']
no_proxy        "localhost, 10.*, *.m086.local, *.m086, *.m286"

require         'rest-client'
RestClient.proxy = ENV['http_proxy']
# RestClient.no_proxy = "localhost, 10.*, *.m086.local, *.m086, *.m286"
