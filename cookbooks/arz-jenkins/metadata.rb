name             'arz-jenkins'
maintainer       'ARZ Allgemeines Rechenzentrum GmbH'
maintainer_email 'christian.bitschnau@arz.at'
license          'Apache 2.0'
description      'Installs/Configures arz-jenkins'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends          'jenkins'

